DATA = load 'hdfs://127.0.0.1:9000/user/piginput/weather_data.txt' using PigStorage(',') as (city:chararray,temperature:int,year:int);
FILTERED = FILTER DATA BY temperature > 35;
DUMP FILTERED;