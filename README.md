# Integration of Spring Framework with Hadoop via Apache Pig #

In this repo you're gonna find complete example of how to process HDFS data in Spring Framework via Pig Latin script. 

Steps:

1) We need to have the pig server in MAPREDUCE mode to be running inside of the Spring application context, for this to happen, we need to attach the Hadoop's CONF directory into the class path. In eclipse, simply add that folder via "add external class folder" in libraries settings. Omitting this will results into  

**ERROR 4010: Cannot find hadoop configurations in classpath (neither hadoop-site.xml nor core-site.xml was found in the classpath)**

2) Now just start the App class in the project analyzing the weather data again. Script prints all rows with temperatures higher then 35C.

For input data:

```
Marid,43,1997
Paris,33,1999
Prague,35,1988
```

you should see:
```
.
.
15/07/27 00:03:36 INFO mapReduceLayer.MapReduceLauncher: Success!
15/07/27 00:03:36 INFO Configuration.deprecation: fs.default.name is deprecated. Instead, use fs.defaultFS
15/07/27 00:03:36 INFO Configuration.deprecation: mapreduce.job.counters.limit is deprecated. Instead, use mapreduce.job.counters.max
15/07/27 00:03:36 INFO Configuration.deprecation: mapred.job.tracker is deprecated. Instead, use mapreduce.jobtracker.address
15/07/27 00:03:36 INFO Configuration.deprecation: dfs.permissions is deprecated. Instead, use dfs.permissions.enabled
15/07/27 00:03:36 INFO Configuration.deprecation: io.bytes.per.checksum is deprecated. Instead, use dfs.bytes-per-checksum
15/07/27 00:03:36 WARN data.SchemaTupleBackend: SchemaTupleBackend has already been initialized
15/07/27 00:03:36 INFO input.FileInputFormat: Total input paths to process : 1
15/07/27 00:03:36 INFO util.MapRedUtil: Total input paths to process : 1
(Madrid,43,1997)
```


enjoy, 

T.


PS: Pig latin scripting is fine, but I'd stick with Hive, since I'm more JDBC guy...:)